<?php

namespace App\Repository;

use App\Entity\Rdv;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Rdv|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rdv|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rdv[]    findAll()
 * @method Rdv[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RdvRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Rdv::class);
    }

    /**
    * @return Rdv[] Returns an array of Rdv objects
    */
    public function findAllArray()
    {
        return $this->createQueryBuilder('r')
            ->select('r')
            ->orderBy('r.id', 'DESC')
            ->getQuery()
            ->getArrayResult();
    }
}
