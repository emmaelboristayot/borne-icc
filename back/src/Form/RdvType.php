<?php

namespace App\Form;

use App\Entity\Rdv;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\{TextType, TelType, EmailType, ChoiceType};

class RdvType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('email', EmailType::class)
            ->add('telephone', TelType::class)
            ->add('sujet',  ChoiceType::class, array(
                'choices'  => array(
                    'Un soutien spirituel' => 'Un soutien spirituel',
                    'Un problème de santé' => 'Un problème de santé',
                    'Un suivi émotionnel' => 'Un suivi émotionnel',
                    'Un conseil familial (fiançailles, difficultés conjugales, etc.)' => 'Un conseil familial (fiançailles, difficultés conjugales, etc.)',
                    'Une présentation d\'enfant' => 'Une présentation d\'enfant',
                    'Recevez la visite d\'un leader chez vous' => 'Recevez la visite d\'un leader chez vous',
                    'Accompagnement professionel' => 'Accompagnement professionel',
                    'Etre intégré' => 'Etre intégré',
                    'Etre écouté' => 'Etre écouté',
                    'Autres' => 'Autres'
                )))
            ->add('commentaire', TextType::class)
            ->add('genre', ChoiceType::class, array(
                'choices'  => array(
                    'Homme' => 'homme',
                    'Femme' => 'femme',
                )))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Rdv::class,
        ]);
    }
}
