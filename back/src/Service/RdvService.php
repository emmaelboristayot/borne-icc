<?php

namespace App\Service;

use App\Entity\Rdv;

class RdvService
{
    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig, $emailSender, $emailPastor)
    {
        $this->mailer       = $mailer;
        $this->twig         = $twig;
        $this->emailSender = $emailSender;
        $this->emailPastor = $emailPastor;
    }

    public function sendEmailToUser($rdv)
    {
        $message = (new \Swift_Message('Votre demande de rendez-vous.'))
        ->setFrom($this->emailSender)
        ->setTo($rdv->getEmail())
        ->setBody(
            $this->twig->render(
                'emails/user-confirm.html.twig',
                array('rdv' => $rdv)
            ),
            'text/html'
        );

        return $this->mailer->send($message) > 0;
    }

    public function sendEmailToPastor($rdv)
    {
        $message = (new \Swift_Message('Nouvelle demande de rendez-vous.'))
        ->setFrom($this->emailSender)
        ->setTo($this->emailPastor)
        ->setBody(
            $this->twig->render(
                'emails/pastor-confirm.html.twig',
                array('rdv' => $rdv)
            ),
            'text/html'
        );

        return $this->mailer->send($message) > 0;
    }

}
