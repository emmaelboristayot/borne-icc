<?php

namespace App\Controller;

use App\Entity\Rdv;
use App\Form\RdvType;
use App\Repository\RdvRepository;
use App\Service\RdvService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class RdvController extends Controller
{
    /**
     * @Route("/", name="rdv_index", methods="GET")
     */
    public function index(RdvRepository $rdvRepository): Response
    {
        return $this->json($rdvRepository->findAllArray());
    }

    /**
     * @Route("/new", name="rdv_new", methods="POST")
     */
    public function new(Request $request, RdvService $rdvService): Response
    {
        $rdv = new Rdv();
        $form = $this->createForm(RdvType::class, $rdv);
        $form->submit(json_decode($request->getContent(), true));


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($rdv);
            $em->flush();

            $rdvService->sendEmailToUser($rdv);
            $rdvService->sendEmailToPastor($rdv);

            return $this->json($rdv);
        }

        return $this->json($form->getErrors(true));
    }

    /**
     * @Route("/{id}", name="rdv_show", methods="GET")
     */
    public function show(Rdv $rdv): Response
    {
        return $this->json($rdv);
    }

    /**
     * @Route("/{id}/edit", name="rdv_edit", methods="POST")
     */
    public function edit(Request $request, Rdv $rdv): Response
    {
        $form = $this->createForm(RdvType::class, $rdv);
        $form->submit(json_decode($request->getContent(), true));

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->json($rdv);
        }

        return $this->json($form->getErrors(true));
    }

    /**
     * @Route("/{id}", name="rdv_delete", methods="DELETE")
     */
    public function delete(Request $request, Rdv $rdv, RdvRepository $rdvRepository): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($rdv);
        $em->flush();

        return $this->json($rdvRepository->findAllArray());
    }
}
