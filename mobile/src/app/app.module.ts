import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { HttpClientModule } from '@angular/common/http';

import { RdvFormProvider } from '../providers/rdv-form.provider';
import { HomePageModule } from '../pages/home/home.module';
import { NosProgrammesPageModule } from '../pages/nos-programmes/nos-programmes.module';
import { RdvFormPageModule } from '../pages/demandes-rdv/rdv-form/rdv-form.module';
import { DemandesRdvPageModule } from '../pages/demandes-rdv/demandes-rdv.module';
import { NosFormationsPageModule } from '../pages/nos-formations/nos-formations.module';
import { GroupesImpactPageModule } from '../pages/groupes-impact/groupes-impact.module';
import { FaqPageModule } from '../pages/faq/faq.module';
import { NosValeursPageModule } from '../pages/nos-valeurs/nos-valeurs.module';
import { NotreVisionPageModule } from '../pages/notre-vision/notre-vision.module';

//Cordova
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Insomnia } from '@ionic-native/insomnia';


//Module


@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    HomePageModule,
    NosProgrammesPageModule,
    DemandesRdvPageModule,
    NosFormationsPageModule,
    GroupesImpactPageModule,
    FaqPageModule,
    NosValeursPageModule,
    NotreVisionPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Insomnia,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RdvFormProvider
  ]
})
export class AppModule {}
