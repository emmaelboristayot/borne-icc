import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NosFormationsPage } from './nos-formations';

@NgModule({
  declarations: [
    NosFormationsPage,
  ],
  imports: [
    IonicPageModule.forChild(NosFormationsPage),
  ],
})
export class NosFormationsPageModule {}
