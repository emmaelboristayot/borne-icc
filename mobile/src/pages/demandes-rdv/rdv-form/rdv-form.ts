import { Component } from "@angular/core";
import { IonicPage, Platform, NavParams, ViewController } from "ionic-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { RdvFormProvider } from "../../../providers/rdv-form.provider";

//@IonicPage()
@Component({
  selector: "rdv-form",
  templateUrl: "rdv-form.html"
})
export class RdvFormPage {
  rdv: FormGroup = this.fb.group({
    genre: ["", [Validators.required]],
    nom: ["", [Validators.required]],
    prenom: ["", [Validators.required]],
    telephone: ["", [Validators.required, Validators.pattern('[0-9]+')]],
    email: ["", [Validators.required, Validators.email]],
    sujet: ["", [Validators.required]],
    commentaire: ["", []]
  });

  getRdv = (key: string) => {
    switch (key) {
      case "rdv_pas":
        return "Rendez-vous de soins pastoraux";
      case "rdv_lea":
        return "Recevez la visite d'un leader chez vous";
      case "rdv_pro":
        return "Accompagnement professionel";
    }
  };

  rdvTypeName: string = this.getRdv(this.params.get("rdvType"));

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController,
    private fb: FormBuilder,
    private rdvFormProvider: RdvFormProvider
  ) {}

  ionViewDidLoad() {}

  onSubmit() {
    this.rdvFormProvider.takeRdv(this.rdv.value).subscribe((data) => console.log(data));
  }
}
