import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RdvFormPage } from './rdv-form';

@NgModule({
    declarations: [
        RdvFormPage
    ],
    imports: [IonicPageModule.forChild(RdvFormPage)],
    exports: [
    ]
})
export class RdvFormPageModule { }
