import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DemandesRdvPage } from './demandes-rdv';
import { RdvFormPageModule } from './rdv-form/rdv-form.module';

@NgModule({
  declarations: [
    DemandesRdvPage,
  ],
  imports: [
    IonicPageModule.forChild(DemandesRdvPage),
    RdvFormPageModule
  ],
})
export class DemandesRdvPageModule {}
