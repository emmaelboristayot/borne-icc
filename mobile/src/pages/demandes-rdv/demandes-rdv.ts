import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  ModalController
} from "ionic-angular";
import { RdvFormPage } from "./rdv-form/rdv-form";

@Component({
  selector: "page-demandes-rdv",
  templateUrl: "demandes-rdv.html"
})
export class DemandesRdvPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController
  ) {}

  ionViewDidLoad() {
  }

  openPage(page): void {
    this.navCtrl.push(RdvFormPage, { rdvType: page });
  }
}
