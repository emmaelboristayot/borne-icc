import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
//Pages
import { NosProgrammesPage } from '../nos-programmes/nos-programmes';
import { DemandesRdvPage } from '../demandes-rdv/demandes-rdv';
import { NotreVisionPage } from '../notre-vision/notre-vision';
import { FaqPage } from '../faq/faq';
import { GroupesImpactPage } from '../groupes-impact/groupes-impact';
import { NosFormationsPage } from '../nos-formations/nos-formations';
import { NosValeursPage } from '../nos-valeurs/nos-valeurs';

//@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public pages: Array<{ title: string, subtitle: string, color: string, component: any }>;

  constructor(public navCtrl: NavController) {


    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Nos programmes', subtitle: '', color: '#303F9F', component: NosProgrammesPage },
      { title: 'Demander un RDV', subtitle: 'Besoin d\'aides ou d\'accompagenement ?', color: '#CDDC39', component: DemandesRdvPage },
      { title: 'Nos valeurs', subtitle: '', color: '#FF4081', component: NosValeursPage },
      { title: 'Nos formations', subtitle: '', color: '#009688', component: NosFormationsPage },
      { title: 'Groupes d\'impact', subtitle: '', color: '#757575', component: GroupesImpactPage },
      { title: 'Notre vision', subtitle: '', color: '#212121', component: NotreVisionPage },
      { title: 'FAQ', subtitle: '', color: '#536DFE', component: FaqPage }
    ];

  }

  openPage(page): void {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.navCtrl.push(page.component);
  }

}
