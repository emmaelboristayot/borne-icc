import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NosValeursPage } from './nos-valeurs';

@NgModule({
  declarations: [
    NosValeursPage,
  ],
  imports: [
    IonicPageModule.forChild(NosValeursPage),
  ],
})
export class NosValeursPageModule {}
