import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupesImpactPage } from './groupes-impact';

@NgModule({
  declarations: [
    GroupesImpactPage,
  ],
  imports: [
    IonicPageModule.forChild(GroupesImpactPage)
  ],
})
export class GroupesImpactPageModule {}
