import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotreVisionPage } from './notre-vision';

@NgModule({
  declarations: [
    NotreVisionPage,
  ],
  imports: [
    IonicPageModule.forChild(NotreVisionPage),
  ],
})
export class NotreVisionPageModule {}
