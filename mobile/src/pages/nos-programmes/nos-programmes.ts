import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-nos-programmes',
  templateUrl: 'nos-programmes.html',
})
export class NosProgrammesPage {

  public programme: string = "hebdomadaire";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
  }

}
