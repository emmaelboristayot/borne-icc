import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NosProgrammesPage } from './nos-programmes';

@NgModule({
  declarations: [
    NosProgrammesPage,
  ],
  imports: [
    IonicPageModule.forChild(NosProgrammesPage),
  ],
})
export class NosProgrammesPageModule {}
