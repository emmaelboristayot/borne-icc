import { NgModule } from '@angular/core';
import { UiBoxComponent } from './ui-box/ui-box';
import { FormationCardComponent } from './formation-card/formation-card';
import { FormationFormComponent } from './formation-form/formation-form';
import { CommonModule } from '@angular/common';
@NgModule({
    declarations: [
        UiBoxComponent,
        FormationCardComponent,
        FormationFormComponent
    ],
    imports: [CommonModule],
    exports: [
        UiBoxComponent,
        FormationCardComponent,
        FormationFormComponent
    ]
})
export class ComponentsModule { }
