import { Component } from '@angular/core';

/**
 * Generated class for the FormationCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'formation-card',
  templateUrl: 'formation-card.html'
})
export class FormationCardComponent {

  text: string;

  constructor() {
    console.log('Hello FormationCardComponent Component');
    this.text = 'Hello World';
  }

}
