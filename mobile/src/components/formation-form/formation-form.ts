import { Component } from '@angular/core';

/**
 * Generated class for the FormationFormComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'formation-form',
  templateUrl: 'formation-form.html'
})
export class FormationFormComponent {

  text: string;

  constructor() {
    console.log('Hello FormationFormComponent Component');
    this.text = 'Hello World';
  }

}
