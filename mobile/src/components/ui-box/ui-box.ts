import { Component, Input } from '@angular/core';

/**
 * Generated class for the UiBoxComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'ui-box',
  templateUrl: 'ui-box.html'
})
export class UiBoxComponent {

  @Input() page: object;

  constructor() {
  }

}
